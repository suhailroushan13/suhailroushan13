
<h1 align="center">Suhail Roushan</h1>
<h2 align="center">Fellow at CS.CODE.IN Class of 2021 </h2>


 <br>Hi there! <img src="https://user-images.githubusercontent.com/42378118/110234147-e3259600-7f4e-11eb-95be-0c4047144dea.gif" width="20"><br>
I'm [𝗦𝘂𝗵𝗮𝗶𝗹 𝗥𝗼𝘂𝘀𝗵𝗮𝗻](https://suhailroushan.com) a 20-Year-Old Junior with an Undergraduate in 𝗖𝗼𝗺𝗽𝘂𝘁𝗲𝗿 𝗦𝗰𝗶𝗲𝗻𝗰𝗲 𝗘𝗻𝗴𝗶𝗻𝗲𝗲𝗿𝗶𝗻𝗴 from 𝗟𝗼𝗿𝗱𝘀 𝗜𝗻𝘀𝘁𝗶𝘁𝘂𝘁𝗲 𝗼𝗳 𝗘𝗻𝗴𝗶𝗻𝗲𝗲𝗿𝗶𝗻𝗴 𝗮𝗻𝗱 𝗧𝗲𝗰𝗵𝗻𝗼𝗹𝗼𝗴𝘆,Hyderabad.<br>

𝗖𝗮𝗻 𝗜 𝗛𝗲𝗹𝗽 𝗬𝗼𝘂?<br>
Having an Experience in 𝗙𝘂𝗹𝗹 𝗦𝘁𝗮𝗰𝗸 𝗗𝗲𝘃𝗲𝗹𝗼𝗽𝗲𝗿 & 𝗗𝗲𝘀𝗶𝗴𝗻𝗶𝗻𝗴 𝗣𝗿𝗼𝘁𝗼𝘁𝘆𝗽𝗲𝘀 . Love to 𝗘𝗱𝗶𝘁𝗶𝗻𝗴 things Like photos & Video Using 𝗔𝗱𝗼𝗯𝗲 Software Like Photoshop, Illustrator, Premier Pro. A 𝗨𝗜/𝗨𝗫 𝗗𝗲𝘀𝗶𝗴𝗻𝗲𝗿 from Neumorphism and Glassmorphism<br>

𝗔 𝗙𝗲𝘄 𝗪𝗼𝗿𝗱𝘀 𝗔𝗯𝗼𝘂𝘁 𝗺𝗲 :<br>
A Guy with having Creativity + Coding Knowledge to Build Products which would Help people by using them. I would like to Solve problems using Updated Technology and making it more helpful. Programming isn't about what you know; it's about what you can figure out<br>

𝗜𝗻𝘁𝗲𝗿𝗲𝘀𝘁𝗲𝗱 𝗶𝗻 <br>
✔️ Startups🚀 
✔️ Web3.0💻
✔️ Marketing 🤳 
✔️ Designing ✏️ 
✔️ Coding 🧑‍💻 
✔️ Domains🤑 
          

𝗣𝗿𝗼𝗷𝗲𝗰𝘁𝘀 : <br>

[Track My Bus 🚌](https://trackmybus.tech) It's an Application which Tracks the Real-Time Location of College Buses. (Live) ✔️
Available on [Play Store](https://play.google.com/store/apps/details?id=com.w8india.w8)

𝗢𝗻 𝗣𝗿𝗼𝗴𝗿𝗲𝘀𝘀 : <br>

[Mentorpe.com🧑‍🏫](https://suhailroushan.com/progress.html)  (Live 2022-23)  ⏳  <br>
[Metacode.live 🧑‍💻](https://suhailroushan.com/progress.html)  (Live 2022-23)  ⏳  <br>
[Memes.school🎒](https://suhailroushan.com/progress.html)  (Live 2022-23)  ⏳  <br>





---

### 📕 Latest Blog Posts

<!-- BLOG-POST-LIST:START -->
- [Encode Your Messages Using Base64](https://suhailroushan.medium.com/encode-your-messages-using-base64-315d2c0420ce)
- [5 Ways to Swap Numbers In C Programming](https://suhailroushan.medium.com/5-ways-to-swap-numbers-in-c-programming-5b2e6f0cf59f)
- [Make UPI Payments Without Internet](https://suhailroushan.medium.com/yes-you-heard-right-now-make-upi-payments-without-internet-9d831e8b2509)
- [GitHub -Where the world builds software](https://suhailroushan.medium.com/github-where-the-world-builds-software-a4b6cf383824)
- [Unix/Linux Fundamentals](https://suhailroushan.medium.com/unix-linux-fundamentals-7f0a3b1e49b4)
<!-- BLOG-POST-LIST:END -->

➡️ [more blog posts...](https://suhailroushan.medium.com/)

---

<p align="left"> 
  Visitor count<br>
  <img src="https://profile-counter.glitch.me/suhailroushan13/count.svg" />
</p>

<h2 align="left">:handshake: Let's get connected:</h2>

[![Linkedin Badge](https://img.shields.io/badge/-suhailroushan-blue?style=flat-square&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/suhailroushan/)](https://www.linkedin.com/in/suhailroushan/) [![Twitter
Badge](https://img.shields.io/badge/-@suhailroushan13-1ca0f1?style=flat-square&labelColor=1ca0f1&logo=twitter&logoColor=white&link=https://twitter.com/suhailroushan13)](https://twitter.com/suhailroushan13) [![Facebook
Badge](https://img.shields.io/badge/-@suhailroushan13-3b5998?style=flat-square&labelColor=3b5998&logo=facebook&logoColor=white&link=https://www.facebook.com/your.suhailroushan/)](https://www.facebook.com/your.suhailroushan/) [![Instagram
Badge](https://img.shields.io/badge/-@suhailroushan.in-D7008A?style=flat-square&labelColor=D7008A&logo=Instagram&logoColor=white&link=https://www.instagram.com/suhailroushan.in/)](https://www.instagram.com/suhailroushan.in/)
[![Linkedin Badge](https://img.shields.io/badge/-suhailroushan.com-blueviolet?style=flat-square&logo=appveyor&logoColor=white&link=https://suhailroushan.com/)](https://suhailroushan.com/)



